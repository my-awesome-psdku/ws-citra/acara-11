gambar = imread('C:\Users\User\Documents\Mochammad Enrique\WS Citra\Program matlab\Acara-11\ini_Melon.jpg');

% Konversi ke ruang warna HSV
hsv_image = rgb2hsv(gambar);
figure, imshow(hsv_image);

% Mengambil nilai rata-rata H, S, dan V dan disimpan ke excel
nilai_rata_h = mean(mean(hsv_image(:,:,1)));
nilai_rata_s = mean(mean(hsv_image(:,:,2)));
nilai_rata_v = mean(mean(hsv_image(:,:,3)));

fitur_hsv = [nilai_rata_h, nilai_rata_s, nilai_rata_v];
xlswrite('fiturwarna_HSV.xlsx', fitur_hsv); % menyimpan fitur ke file excel
