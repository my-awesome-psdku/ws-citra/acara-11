gambar = imread('C:\Users\User\Documents\Mochammad Enrique\WS Citra\Program matlab\Acara-11\ini_Melon.jpg');

% Konversi ke ruang warna CIELab
lab_image = rgb2lab(gambar);
figure, imshow(lab_image);

% Mengambil nilai rata-rata L*, a*, dan b* dan disimpan ke excel
nilai_rata_L = mean(mean(lab_image(:,:,1)));
nilai_rata_a = mean(mean(lab_image(:,:,2)));
nilai_rata_b = mean(mean(lab_image(:,:,3)));

fitur_cielab = [nilai_rata_L, nilai_rata_a, nilai_rata_b];
xlswrite('fiturwarna_CIELab.xlsx', fitur_cielab); % menyimpan fitur ke file excel
