gambar = imread('C:\Users\User\Documents\Mochammad Enrique\WS Citra\Program matlab\Acara-11\ini_Melon.jpg');
grayscale = rgb2gray(gambar);

edge_image = edge(grayscale, 'sobel');
filled_image = imfill(edge_image, 'holes');

se = strel('disk',1);
eroded_image = imerode(filled_image, se);
contour_image = filled_image - eroded_image;

stats = regionprops(contour_image, 'Area', 'Perimeter');

luas = stats.Area;
perimeter = stats.Perimeter;
faktor_bentuk = (perimeter^2) / (4 * pi * luas);

disp(['Luas objek: ', num2str(luas)]);
disp(['Perimeter objek: ', num2str(perimeter)]);
disp(['Faktor Bentuk: ', num2str(faktor_bentuk)]);

% Visualisasi citra hasil morfologi
figure;
imshow(contour_image);
title('Citra Hasil Morfologi');

% Tampilkan grid tabel dengan label sumbu
hold on;
grid on;
axis on;

% Hitung jumlah baris dan kolom pada citra
jumlah_baris = size(contour_image, 1);
jumlah_kolom = size(contour_image, 2);

% Tentukan jumlah grid yang ingin ditampilkan
jumlah_grid = 10; % Misalnya, ingin menampilkan 10 grid

% Hitung jarak antara grid
jarak_grid_x = floor(jumlah_kolom / jumlah_grid);
jarak_grid_y = floor(jumlah_baris / jumlah_grid);

% Gambar grid dan label sumbu x dan y
for i = 1:jumlah_grid
    y = i * jarak_grid_y;
    plot([0, jumlah_kolom], [y, y], 'y', 'LineWidth', 1.5); % Ganti warna menjadi kuning
    
    x = i * jarak_grid_x;
    plot([x, x], [0, jumlah_baris], 'y', 'LineWidth', 1.5); % Ganti warna menjadi kuning
    
    % Tambahkan label sumbu y
    text(5, y, num2str(y), 'HorizontalAlignment', 'right', 'Color', 'r');
    
    % Tambahkan label sumbu x
    text(x, 5, num2str(x), 'VerticalAlignment', 'top', 'HorizontalAlignment', 'center', 'Color', 'r');
end

% Tambahkan panah pada titik chain code
x_chain = [100 120];
y_chain = [200 180];

% Gambar panah
quiver(x_chain(1), y_chain(1), x_chain(2)-x_chain(1), y_chain(2)-y_chain(1), 0, 'g', 'LineWidth', 1.5);

hold off;

% Hitung sumbu-y (jumlah grid)
sumbu_y = jumlah_grid;
disp(['Jumlah Sumbu-y (jumlah grid): ', num2str(sumbu_y)]);
